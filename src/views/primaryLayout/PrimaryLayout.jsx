import React, { useEffect, useState } from "react";
import "./primaryLayout.css";
import RegisterForm from "../../components/hybrids/forms/registerForm";
import CarForm from "../../components/hybrids/forms/carForm";
import InsuranceForm from "../../components/hybrids/forms/insuranceForm";
import DiscountForm from "../../components/hybrids/forms/discountForm";
import H5 from "../../components/infrastructure/typography/H5/H5";
import Button from "../../components/infrastructure/button";
import arrowIcon from "../../assets/images/arrow.svg";
import { REGISTER_TITLE, THIRD_PARTY_INSURANCE_TITLE } from "./constants";
import { registerFormValidation } from "../../utils/validation";
import Dialog from "../../components/infrastructure/dialog";
import Header from "../../components/hybrids/header";
export default function PrimaryLayout() {
  const INITIAL_USER_FORM = {
    userName: "",
    userFamily: "",
    phoneNumber: "",
    password: "",
  };
  const [userData, setUserData] = useState(INITIAL_USER_FORM);
  const [orderData, setOrderData] = useState({});
  const [errors, setErrors] = useState([]);
  const [isUserLoggedIn, setIsUserLoggedIn] = useState();
  const [currentStep, setCurrentStep] = useState(0);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isDisabledNextStep, setIsDisabledNextStep] = useState(true);

  const { userName, userFamily } = userData;

  const USER_FULL_NAME = `${userName} ${userFamily}`;

  useEffect(() => {
    if (currentStep === 1 && orderData?.brand && orderData?.model) {
      return setIsDisabledNextStep(false);
    }
    if (currentStep === 2 && orderData?.insurance) {
      return setIsDisabledNextStep(false);
    }
    // TODO
    // if (
    //   currentStep === 3 &&
    //   orderData?.thirdDiscount &&
    //   orderData?.driverDiscount
    // ) {
    //   return setIsDisabledNextStep(false);
    // }
    setIsDisabledNextStep(true);
  }, [orderData]);

  const onChange = (event, formType) => {
    const { name, value } = event?.target;
    if (formType === "register") {
      return setUserData({ ...userData, [name]: value });
    }
    setOrderData({ ...orderData, [name]: value });
  };

  const nextFormSetter = () => {
    if (currentStep === 3) {
      return setIsDialogOpen(true);
    }
    setIsDisabledNextStep(true);
    setCurrentStep(currentStep + 1);
  };

  const previousFormSetter = () => {
    if (currentStep === 1) {
      successLogout();
    }
    setCurrentStep(currentStep - 1);
  };

  const successLogin = () => {
    setIsUserLoggedIn(true);
    nextFormSetter();
  };

  const successLogout = () => {
    setIsUserLoggedIn(false);
  };

  const handleSubmitForm = () => {
    const validationResult = registerFormValidation(userData);
    if (validationResult?.length > 0) {
      return setErrors(validationResult);
    }
    setErrors([]);
    successLogin();
  };

  const forms = {
    0: <RegisterForm onChange={onChange} type="register" userData={userData} />,
    1: <CarForm onChange={onChange} orderData={orderData} />,
    2: <InsuranceForm onChange={onChange} orderData={orderData} />,
    3: <DiscountForm onChange={onChange} orderData={orderData} />,
  };

  const hasSingleButton = !currentStep || currentStep === 3;

  const handleCloseDialog = () => {
    setIsDialogOpen(false);
  };

  // TODO - for key should be used unique ID or UUID package
  const renderErrors = errors?.map((error, index) => (
    <div key={index} className="validation-error">
      {error}
    </div>
  ));

  return (
    <div className="app-container">
      {isDialogOpen && <Dialog onClose={handleCloseDialog} data={orderData} />}
      <Header isUserLoggedIn={isUserLoggedIn} userLabel={USER_FULL_NAME} />
      <div className="primary-layout-container">
        <div className="right-section">
          <div className="form-container">
            <div className="form-content">
              <H5 className="form-title">
                {currentStep === 0
                  ? REGISTER_TITLE
                  : THIRD_PARTY_INSURANCE_TITLE}
              </H5>
              {forms[currentStep]}
              {renderErrors}
              <div
                className={`${
                  !currentStep ? "form-button" : "form-control-space"
                }`}
              >
                {hasSingleButton && (
                  <Button
                    onClick={handleSubmitForm}
                    variant="primary"
                    label={currentStep === 3 ? "استعلام قیمت" : "ثبت نام"}
                  />
                )}
                {!hasSingleButton && (
                  <>
                    <Button
                      onClick={previousFormSetter}
                      isReverse={true}
                      icon={arrowIcon}
                      label="بازگشت"
                      variant="outline"
                    />
                    <Button
                      isDisabled={isDisabledNextStep}
                      onClick={nextFormSetter}
                      icon={arrowIcon}
                      label="مرحله ی بعد"
                      variant="outline"
                    />
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="left-section" />
        <div className="green-car-container">
          <div className="green-car" />
        </div>
      </div>
    </div>
  );
}
