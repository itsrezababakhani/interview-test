import axios from "axios";
const BASE_URL = "https://bimito.com/api/product/";
export const getRequest = (endPoint) => {
  return axios.get(BASE_URL + endPoint);
};
