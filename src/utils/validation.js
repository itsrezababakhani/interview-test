const USER_NAME_ERROR = "نام کاربری باید کاملا فارسی باشد.";
const USER_FAMILY_ERROR = "نام خانوادگی باید کاملا فارسی باشد.";
const FILL_FIELDS_ERROR = "تمامی فیلد ها باید با مقادیر صحیح پر باشد.";
const MIN_PASSWORD_LENGTH_ERROR = "رمز عبور حداقل باید ۴ حرف باشد.";
const MAX_PASSWORD_LENGTH_ERROR = "رمز عبور حداکثر باید ۱۰ حرف باشد.";
const UPPER_CASE_PASSWORD_ERROR = "رمز عبور باید حداقل ۱ حرف بزرگ داشته باشد.";
const LOWER_CASE_PASSWORD_ERROR = "رمز عبور باید یک حرف کوچک داشته باشد.";
const NUMBER_PASSWORD_ERROR = "رمز عبور باید حداقل یک عدد داشته باشد.";

const hasUpperCaseRegExp = "(?=.*[A-Z])";

const hasLowerCaseRegExp = "(?=.*[a-z])";

const hasNumberRegExp = "(?=.*[0-9])";

const passwordChecker = (password) => {
  const passwordErrors = [];
  if (password?.length < 4) {
    passwordErrors.push(MIN_PASSWORD_LENGTH_ERROR);
  }
  if (password?.length > 10) {
    passwordErrors.push(MAX_PASSWORD_LENGTH_ERROR);
  }
  // checking upper case letter
  const upperCaseRegExp = new RegExp(hasUpperCaseRegExp);
  const hasUpperCaseWord = upperCaseRegExp.test(password);
  if (!hasUpperCaseWord) passwordErrors.push(UPPER_CASE_PASSWORD_ERROR);
  // checking lower case letter
  const lowerCaseRegExp = new RegExp(hasLowerCaseRegExp);
  const hasLowerCaseWord = lowerCaseRegExp.test(password);
  if (!hasLowerCaseWord) passwordErrors.push(LOWER_CASE_PASSWORD_ERROR);
  // checking number
  const numberRegExp = new RegExp(hasNumberRegExp);
  const hasNumber = numberRegExp.test(password);
  if (!hasNumber) passwordErrors.push(NUMBER_PASSWORD_ERROR);
  return passwordErrors;
};

/**
 *
 * @param {String} entryString - entry data for validation
 * @returns {Boolean}
 */
const persianChecker = (entryString) => {
  const persianRegex = new RegExp("[\u0600-\u06FF]");
  return persianRegex.test(entryString) === true;
};

export const registerFormValidation = (userData) => {
  const errors = [];
  const { password, userName, userFamily, phoneNumber } = userData;
  const hasEmptyField =
    password === "" ||
    userFamily === "" ||
    userName === "" ||
    phoneNumber === "";
  if (hasEmptyField) errors.push(FILL_FIELDS_ERROR);
  const isPersianUserName = persianChecker(userName);
  if (!isPersianUserName) errors.push(USER_NAME_ERROR);
  const isPersianUserFamily = persianChecker(userFamily);
  if (!isPersianUserFamily) errors.push(USER_FAMILY_ERROR);
  const passwordCheckingResult = passwordChecker(password);
  if (passwordCheckingResult?.length > 0) {
    passwordCheckingResult?.map((errorMessage) => errors.push(errorMessage));
  }
  return errors;
};
