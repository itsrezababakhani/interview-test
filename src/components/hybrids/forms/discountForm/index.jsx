import React, { useEffect, useState } from "react";
import { getRequest } from "../../../../services/httpService";
import Select from "../../../infrastructure/select";
import Descriptions from "../../../infrastructure/typography/Description/Descriptions";
import "./discountForm.css";
export default function DiscountForm({ onChange }) {
  const [thirdDiscount, setThirdDiscount] = useState([]);
  const [driverDiscount, setDriverDiscount] = useState([]);
  useEffect(() => {
    getRequest("third/third-discounts")
      .then((response) => {
        setThirdDiscount(response?.data);
      })
      .catch((err) => {
        // TODO - Error Handling Should be implement
      });
    getRequest("third/driver-discounts")
      .then((response) => {
        setDriverDiscount(response?.data);
      })
      .catch((err) => {
        // TODO - Error Handling Should be implement
      });
  }, []);
  return (
    <>
      <Descriptions className="discount-form-description">
        درصد تخفیف بیمه شخص ثالث و حوادث راننده را وارد کنید.
      </Descriptions>
      <div className="discount-form-container">
        <Select
          defaultLabel="درصد تخفیف ثالث"
          defaultValue=""
          name="thirdDiscount"
          onChange={onChange}
          options={thirdDiscount}
        />
        <Select
          defaultLabel="درصد تخفیف حوادث راننده"
          defaultValue=""
          name="driverDiscount"
          onChange={onChange}
          options={driverDiscount}
        />
      </div>
    </>
  );
}
