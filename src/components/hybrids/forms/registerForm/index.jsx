import React from "react";
import TextField from "../../../infrastructure/textField";
import "./register.css";
export default function RegisterForm({ onChange, type, userData }) {
  const { userName, userFamily, phoneNumber, password } = userData;
  const handleChangeTextFields = (event) => {
    onChange?.(event, type);
  };
  return (
    <div className="register-form-container">
      <div className="name-fields-section">
        <TextField
          value={userName}
          onChange={handleChangeTextFields}
          name="userName"
          className="name-text-field"
          placeholder="نام"
        />
        <TextField
          value={userFamily}
          onChange={handleChangeTextFields}
          name="userFamily"
          placeholder="نام خانوادگی"
        />
      </div>
      <TextField
        value={phoneNumber}
        onChange={handleChangeTextFields}
        name="phoneNumber"
        type="tel"
        placeholder="شماره موبایل"
      />
      <TextField
        value={password}
        onChange={handleChangeTextFields}
        name="password"
        type="password"
        placeholder="رمز عبور"
      />
    </div>
  );
}
