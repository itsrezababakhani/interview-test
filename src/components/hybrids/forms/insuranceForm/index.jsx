import React, { useState, useEffect } from "react";
import { getRequest } from "../../../../services/httpService";
import Select from "../../../infrastructure/select";
import Descriptions from "../../../infrastructure/typography/Description/Descriptions";
import "./insurance.css";
export default function InsuranceForm({ onChange }) {
  const [companies, setCompanies] = useState([]);
  useEffect(() => {
    getRequest("third/companies")
      .then((response) => {
        setCompanies(response?.data);
      })
      .catch((err) => {
        // TODO - Error Handling Should be implement
      });
  }, []);
  return (
    <>
      <Descriptions className="insurance-description">
        شرکت بیمه گر قبلی خود را در این بخش وارد کنید.
      </Descriptions>
      <Select
        defaultValue=""
        defaultLabel="شرکت بیمه گر قبلی"
        name="insurance"
        onChange={onChange}
        options={companies}
      />
    </>
  );
}
