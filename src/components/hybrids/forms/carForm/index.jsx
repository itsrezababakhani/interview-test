import React, { useEffect, useState } from "react";
import Select from "../../../infrastructure/select";
import "./carForm.css";
import { getRequest } from "../../../../services/httpService";
import Descriptions from "../../../infrastructure/typography/Description/Descriptions";
export default function CarForm({ onChange, orderData }) {
  const [brands, setBrands] = useState([]);
  const [selectedBrand, setSelectedBrand] = useState(orderData?.brand);
  const [model, setModel] = useState([]);
  useEffect(() => {
    getRequest("vehicle/models/third")
      .then((response) => {
        setBrands(response?.data?.[0]?.brands);
      })
      .catch((err) => {
        // TODO - Error Handling Should be implement
      });
  }, []);

  useEffect(() => {
    // [TODO] - it should be fixed when calling prev page function we should set select box value again
    const filtered = brands?.filter((current) => {
      return current?.title.toString() === selectedBrand;
    });
    setModel(filtered?.[0]?.models);
  }, [selectedBrand]);

  return (
    <>
      <Descriptions className="car-form-description">
        نوع و مدل خودروی خود را انتخاب کنید.
      </Descriptions>
      <div className="car-form-container">
        <Select
          value={orderData?.brand}
          className="brand-select-box"
          defaultValue=""
          defaultLabel="نوع خودرو"
          options={brands}
          name="brand"
          onChange={(event) => {
            onChange?.(event);
            setSelectedBrand(event?.target?.value);
          }}
        />
        <Select
          value={orderData?.model}
          isDisabled={selectedBrand === ""}
          defaultValue=""
          defaultLabel="مدل خودرو"
          name="model"
          onChange={onChange}
          options={model}
        />
      </div>
    </>
  );
}
