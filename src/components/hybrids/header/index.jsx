import React from "react";
import "./header.css";
import logo from "../../../assets/images/logo.svg";
import Avatar from "../../infrastructure/avatar";
export default function Header({ isUserLoggedIn, userLabel }) {
  return (
    <div className="header-container">
      <div className="header-content">
        <img alt="bimito-logo" className="logo" src={logo} />
        <div className="header-title">سامانه ی مقایسه و خرید آنلاین بیمه</div>
        <div>{isUserLoggedIn ? <Avatar label={userLabel} /> : "ثبت نام"}</div>
      </div>
    </div>
  );
}
