import React from "react";
import "./dialog.css";

export default function Dialog({ data, onClose }) {
  const { brand, model, insurance, thirdDiscount, driverDiscount } = data;
  return (
    <div onClick={onClose} className="dialog-container">
      <div className="dialog-content">
        <div className="info-items">
          <span>برند خودرو</span> <span>{brand}</span>
        </div>
        <div className="info-items">
          <span>مدل خودرو</span> <span>{model}</span>
        </div>
        <div className="info-items">
          <span>بیمه کننده ی قبلی</span> <span>{insurance}</span>
        </div>
        <div className="info-items">
          <span>درصد تخفیف شخص ثالث</span> <span>{thirdDiscount}</span>
        </div>
        <div className="info-items">
          <span>درصد تخفیف حوادث</span> <span>{driverDiscount}</span>
        </div>
      </div>
    </div>
  );
}
