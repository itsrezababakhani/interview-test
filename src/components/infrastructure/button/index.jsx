import React from "react";
import "./button.css";
import arrow from "../../../assets/images/arrow.svg";
export default function Button({
  label,
  variant,
  icon,
  className,
  isReverse,
  onClick,
  isDisabled,
}) {
  return (
    <button
      disabled={isDisabled}
      onClick={onClick}
      className={`${variant}-button ${className ?? ""}`}
    >
      {label}
      {icon && (
        <img
          className={`${isReverse ? "reverse-icon" : ""}`}
          src={arrow}
          alt="arrow-icon"
        />
      )}
    </button>
  );
}
