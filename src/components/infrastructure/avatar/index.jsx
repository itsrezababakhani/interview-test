import React from "react";
import userImage from "../../../assets/images/user.svg";
import "./avatar.css";
export default function index({ label }) {
  return (
    <div className="user-avatar-container">
      <img className="user-icon" src={userImage} alt="" />
      <span>{label}</span>
    </div>
  );
}
