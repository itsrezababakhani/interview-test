import React from "react";
import "./select.css";
export default function Select({
  name,
  options,
  onChange,
  defaultValue,
  defaultLabel,
  isDisabled,
  className,
  value,
}) {
  const renderOptions = options?.map((current) => {
    return (
      <option key={current?.id} value={current?.title}>
        {current?.title}
      </option>
    );
  });

  return (
    <select
      value={value}
      disabled={isDisabled}
      onChange={onChange}
      className={`${className ?? ""} select-container`}
      name={name}
    >
      {defaultLabel && (
        <option className="default-selected-option" value={defaultValue}>
          {defaultLabel}
        </option>
      )}
      {renderOptions}
    </select>
  );
}
