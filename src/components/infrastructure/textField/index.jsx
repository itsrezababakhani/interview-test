import React from "react";
import "./textField.css";
export default function TextField({
  type,
  placeholder,
  className,
  onChange,
  name,
  value,
}) {
  return (
    <input
      value={value}
      name={name}
      className={`text-field ${className ?? ""}`}
      placeholder={placeholder}
      type={type}
      onChange={onChange}
    />
  );
}
