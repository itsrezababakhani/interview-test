import React from "react";
import "./h5.css";
export default function H5({ children, className }) {
  return <h5 className={`h5-typo ${className ?? ""}`}>{children}</h5>;
}
