import React from "react";
import "./description.css";
export default function Descriptions({ children, className }) {
  return (
    <div className={`primary-description ${className ?? ""}`}>{children}</div>
  );
}
